# University of Georgia App Icons and Favicons

## Usage

These icons can be used on any website representing the University of Georgia in an official capacity.

![UGA shield icon](./icons/favicon-144x144.png)
![UGA shield icon](./icons/favicon-72x72.png)
![UGA shield icon](./icons/favicon-36x36.png)
![UGA shield icon](./icons/favicon-16x16.png)

## What’s Included?

* `icons` folder — formatted icons for iOS, Android, Microsoft IE/Edge, and websites
* `manifest.json` — defines web app behavior
* `ieconfig.xml` — defines Microsoft-specific configuration for pinned sites
* `code.html` — sample template code

## Installation Instructions

1. Upload `icons` folder and `manifest.json` and `ieconfig.xml` files to the root of your public web directory, renaming  if conflicts exist.
2. Copy relevant HTML code from `code.html` into your web template.
3. Update `manifest.json` with website-specific [settings](https://developer.mozilla.org/en-US/docs/Web/Manifest).
3. Update paths to uploaded files if renaming of files/folders was necessary.